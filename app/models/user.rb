# == Schema Information
# Schema version: 20110615141143
#
# Table name: users
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime
#  updated_at :datetime
#
require 'digest'

class User < ActiveRecord::Base
  attr_accessor :pass, :pass_conf
  attr_accessible :name, :email, :pass_conf, :pass

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, :presence => true,
                   :length => { :maximum => 50 }
  validates :email, :presence => true,
                    :format => { :with => email_regex },
                    :uniqueness => { :case_sensitive => false }
  validates :pass, :confirmation => true ,
                  :presence => true,
                   :confirmation => true,
                   :length => { :within => 6..40 }
  before_save :encrypt_pass

  

  def has_pass?(sub_pass)
    self.encrypted_pass == encrypt(sub_pass)
  end

  def self.aut(email, sub)
    user = find_by_email(email)
    return nil if user.nil?
    return user if user.has_pass?(sub)
  end

  private 

  def encrypt_pass
    self.salt = make_salt if new_record?
    self.encrypted_pass = encrypt(pass)
  end

  def encrypt(string)
    secure_hash("#{salt}--#{string}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{pass}")
  end




    
end
