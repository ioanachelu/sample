require 'spec_helper'

describe User do
#  pending "add some examples to (or delete) #{__FILE__}"
  before(:each) do
    @attr = {
      :name => "Example User",
      :email => "user@example.com",
      :pass => "foobar",
      :pass_conf => "foobar"
    }
  end

  it "should create a new instance given valid attributes" do
    User.create!(@attr)
  end

  it "should require a name" do
    no_name_user = User.new(@attr.merge(:name => ""))
    no_name_user.should_not be_valid
  end

  it "should require an email" do
    no_email_user = User.new(@attr.merge(:email => ""))
    no_email_user.should_not be_valid
  end

  it "should reject names that are too long" do
    long_name = "a" *51
    long_name = User.new(@attr.merge(:name => long_name))
    long_name.should_not be_valid
  end
  it "should accept valid email addresses" do
    add = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    add.each do |a|
      valid_u = User.new(@attr.merge(:email => a))
      valid_u.should be_valid
    end
  end

  it "should not acccept invalid email addresses" do
    add = %w[user@foo,com user_at_foo.org example.user@foo.]
    add.each do |a|
      invalid_u = User.new(@attr.merge(:email => a))
      invalid_u.should_not be_valid
    end
  end

  it "should reject duplicate addresses" do
    User.create!(@attr)
    user_dup = User.new(@attr)
    user_dup.should_not be_valid
  end
  
  it "should reject email addresses identical up to case" do
    up = @attr[:email].upcase
    User.create!(@attr.merge(:email => up))
    user_dup = User.new(@attr)
    user_dup.should_not be_valid
  end

  describe "pass validations" do

    before (:each) do
      @user = User.create!(@attr)
    end

    it "should require a pass" do
      User.new(@attr.merge(:pass => "", :pass_conf => "")).
        should_not be_valid
    end

    it "should require a matching pass conf" do 
      User.new(@attr.merge(:pass_conf => "invalid")).
        should_not be_valid
    end

    it "should reject short pass" do
      User.new(@attr.merge(:pass => "aaaaa", :pass_conf => "aaaaa")).
        should_not be_valid
    end

    it "should reject long pass" do
      long = "a" * 41
      hash = @attr.merge(:pass => long, :pass_conf => long)
      User.new(hash).should_not be_valid
    end

    it "should have an encrypted pass attr" do
      @user.encrypted_pass.should_not be_blank
    end

    it "should be true if pass match" do
      @user.has_pass?(@attr[:pass]).should be_true
    end

    it "should be false if pass don't match" do
      @user.has_pass?("invalid").should be_false
    end
  end


  describe " auth" do

    it "should return nil on email/pass mismatch" do
      w = User.aut(@attr[:email] , "wrong")
      w.should be_nil
    end

    it "should return nil on email with no user" do
      w = User.aut("altceva@gs.com", @attr[:pass])
      w.should be_nil
    end

    it "should return the user if it matches " do
      w = User.aut(@attr[:email], @attr[:pass])
      w.should == @user
    end
  end



end
